<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Residuos;

class NewSheetListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        if($event->data->count() > 0){

            foreach($event->data as $dado){

                if(!empty($dado['nome'])){

                    $residuo[] = [
                        'nome'        => $dado['nome'],
                        'tipo'        => $dado['tipo'],
                        'categoria'   => $dado['categoria'],
                        'tratamento'  => $dado['tratamento'],
                        'classe'      => $dado['classe'],
                        'unid_medida' => $dado['unid_medida'],
                        'peso'        => str_replace(',', '.', $dado['peso']),
                        'created_at'  => date('Y-m-d H:i:s'),
                        'updated_at'  => date('Y-m-d H:i:s'),
                    ];
                } else {
                    break;
                }
                
            }

            Residuos::insert($residuo);
        }
    }
}
