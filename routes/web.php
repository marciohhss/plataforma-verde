<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Residuos CRUD
Route::get('residuo', 'ResiduosController@index');
Route::get('residuo/{id}', 'ResiduosController@show');
Route::put('residuo/{id}', 'ResiduosController@update');
Route::post('residuo', 'ResiduosController@store');
Route::delete('residuo/{id}', 'ResiduosController@destroy');

