<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Events\NewSheetHasRegisteredEvent;
use App\Residuos;
use Excel;

class ResiduosController extends Controller
{

    public function index()
    {
        //return csrf_token(); 
        return Residuos::all();
    }


    public function store(Request $request)
    {
        $path = public_path('uploads/planilha_residuos.xlsx');
        $data = Excel::load($path)->get();
        
        if(event(new NewSheetHasRegisteredEvent($data))){
            return response()->json(['Dados inseridos com sucesso!']);
        } else {
            return response()->json(['Erro ao inserir dados!']);
        }

    }

    public function show($id)
    {
        return Residuos::findOrfail($id);
    }


    public function update(Request $request, $id)
    {
        $exam = Residuos::findORFail($id);
        $exam->update($request->all());
    }

    public function destroy($id)
    {
        $exam = Residuos::findOrFail($id);
        $exam->delete();
    }
}
