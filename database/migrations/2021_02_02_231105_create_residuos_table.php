<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResiduosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residuos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome',255)->nullable();
            $table->string('tipo',50)->nullable();
            $table->string('categoria',25)->nullable();
            $table->string('tratamento',30)->nullable();
            $table->string('classe',10)->nullable();
            $table->string('unid_medida',5)->nullable();
            $table->decimal('peso', 10, 3)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residuos');
    }
}
