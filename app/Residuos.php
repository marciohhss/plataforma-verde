<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Residuos extends Model
{
    protected $fillable = [
        'nome',
        'tipo',
        'categoria',
        'tratamento',
        'classe',
        'unid_medida',
        'peso'
    ];
}
